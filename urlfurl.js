const request = require('request')
const fs = require('fs')
const writeStream = fs.createWriteStream('./processed/' + process.argv[2])
writeStream.on('finish', () =>
{
	console.log('completed')
})

fs.readFile(process.argv[2], 'utf8', (err, res) =>
{
	var text = res.split('\r')
	var links = []
	text.forEach((e) =>
	{
		var options = {
			uri: e,
			timeout: 2000,
			followAllRedirects: true
		}

		request(options, (err, res) =>
		{
			var originalURL = options.uri
			var redirectURL = (res) ? res.request.uri.href : 'no url'
			links.push(
			{
				original: originalURL,
				redirect: redirectURL
			})
		})
	})
	setTimeout(() =>
	{
		links.forEach((e) =>
		{
			writeStream.write(e.original + ' =>\n' + e.redirect)
			writeStream.write('\n\n')
		})
		writeStream.end()
	}, 3000)
})
